# WillowTree Name Game


This app is a name game that helps you learn the names of the WillowTree employees. It will display names and faces imported from the WillowTree API. It will also keep track of your score, letting you know how many times you have guessed correctly and incorrectly. The different modes include:

  - Normal Mode: Displays all employees that work for WillowTree
  - Matt Mode: Displays the employees that work at WillowTree named Mat(t)
  - Job Mode: Asks the user to guess the person based on their job title

# Front-End Development

  - This app focuses on front-end development, using WillowTree's API
  - The app downloads all necessary data and loads it into an MVC design pattern

# Original Code
All code was written by me, Charles von Lehe, except for 3rd party Cocoapods. These Cocoapods are listed below:

  - Alamofire
  - DropDown

# Architecture
The app loads all details of each individual person from the API when the user changes the game mode. The data is cached at start up to trim down on the number of the API calls as the game is played.


# Other code

Another app that I am particularly proud of is Historic Walking Tour. It is very efficient and has an aesthetically-pleasing UI. It is an app that displays Historical Sites in given cities and plays audio for each site. This code can be found on Bitbucket at the following URL:  https://vonlehec@bitbucket.org/vonlehec/hwt-sb.git.



# About Me
* [Resume](http://docdro.id/T2vHCnS)
* [LinkedIn](https://www.linkedin.com/in/charles-von-lehe-63410a3a/)

