//
//  NameGame.swift
//  NameGame
//
//  Created by Erik LaManna on 11/7/16.
//  Copyright © 2016 WillowTree Apps. All rights reserved.
//

import Foundation
import Alamofire

protocol NameGameDelegate: class {
}

class NameGame {
   var employees:[Employee] = []
   var correct = 0
   var incorrect = 0
    weak var delegate: NameGameDelegate?
   var selectedEmployee:Employee!
   var selectedJobTitle:String!
    let numberPeople = 6
   var gameMode = GameMode.normal

    // Load JSON data from API
   func loadGameData(completion: @escaping () -> Void) {
      guard let url = URL(string: "https://willowtreeapps.com/api/v1.0/profiles/") else {
         completion()
         return
      }
      Alamofire.request(url, method: .get, parameters: nil).responseJSON { (response) in
         guard let dictionaryArray = self.getDictionary(fromData: response.data) else {
            completion()
            return
         }
         for dictionary in dictionaryArray {
            self.employees.append(Employee(dictionary: dictionary))
         }
         completion()
      }
    }
   
   private func getDictionary (fromData:Data?)->[[String:Any]]? {
      guard let data = fromData else {return nil}
      do {
         let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
         return json as? [[String:Any]]
      }catch {
         return nil
      }
   }
   
   public enum GameMode {
      case normal, mattMode, jobMode
   }
   
   func newGame(gameMode:GameMode)->[Employee] {
      self.gameMode = gameMode
      var currentEmployees:[Employee] = []
      while currentEmployees.count < numberPeople {
         let employee = self.employees[Int(arc4random_uniform(UInt32(self.employees.count)))]
         if (!currentEmployees.contains{$0.getName() == employee.getName()}) &&
            (gameMode != .mattMode || (employee.getName()?.contains("Mat"))!) &&
            (gameMode != .jobMode || employee.jobTitle.replacingOccurrences(of: " ", with: "") != "") {
            currentEmployees.append(employee)
         }
      }
      selectedEmployee = currentEmployees[Int(arc4random_uniform(UInt32(currentEmployees.count)))]
      selectedJobTitle = selectedEmployee.jobTitle
      return currentEmployees
   }
}
