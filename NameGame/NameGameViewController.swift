//
//  ViewController.swift
//  NameGame
//
//  Created by Matt Kauper on 3/8/16.
//  Copyright © 2016 WillowTree Apps. All rights reserved.
//

import UIKit
import DropDown

class NameGameViewController: UIViewController {
   var nameGame = NameGame()
    @IBOutlet weak var outerStackView: UIStackView!
    @IBOutlet weak var innerStackView1: UIStackView!
    @IBOutlet weak var innerStackView2: UIStackView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet var imageButtons: [FaceButton]!
   @IBOutlet weak var gameTypeButton: UIButton!
   var dropDown:DropDown!
   @IBOutlet weak var scoreLabel: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
      setupTypeButton()
      nameGame.loadGameData {
         self.setNewGame(gameMode: .normal)
      }
        let orientation: UIDeviceOrientation = self.view.frame.size.height > self.view.frame.size.width ? .portrait : .landscapeLeft
        configureSubviews(orientation)
    }
   
   func setNewGame (gameMode: NameGame.GameMode) {
      var i = 0
      var employees = self.nameGame.newGame(gameMode: gameMode)
      for button in self.imageButtons {
         button.set(employee: employees[i])
         i += 1
      }
      guard let name = nameGame.selectedEmployee.getName() else {return}
      if gameMode == .jobMode, let jobTitle = nameGame.selectedJobTitle {
         questionLabel.text = "Who is a \(jobTitle)?"
      }else {
         questionLabel.text = "Who is \(name)?"
      }
   }
   func setupTypeButton () {
      dropDown = DropDown(anchorView: gameTypeButton)
      dropDown.dataSource = ["Normal", "Matt Mode", "Job Mode"]
      dropDown.selectionAction = {(index: Int, item: String) in
         self.gameTypeButton.setTitle(item + " ▼", for: .normal)
         if item == "Normal" {
            self.setNewGame(gameMode: .normal)
         }else if item == "Matt Mode" {
            self.setNewGame(gameMode: .mattMode)
         }else {
            self.setNewGame(gameMode: .jobMode)
         }
      }
   }

    @IBAction func faceTapped(_ button: FaceButton) {
      let correct = button.employee.id == nameGame.selectedEmployee.id && (nameGame.gameMode != .jobMode || button.employee.jobTitle == nameGame.selectedJobTitle)
      if correct {
         nameGame.correct += 1
      }else {
         nameGame.incorrect += 1
      }
      scoreLabel.text = "Correct: \(nameGame.correct) - Incorrect: \(nameGame.incorrect)"
      button.set(correct: correct)
    }

    func configureSubviews(_ orientation: UIDeviceOrientation) {
        if orientation.isLandscape {
            outerStackView.axis = .vertical
            innerStackView1.axis = .horizontal
            innerStackView2.axis = .horizontal
        } else {
            outerStackView.axis = .horizontal
            innerStackView1.axis = .vertical
            innerStackView2.axis = .vertical
        }

        view.setNeedsLayout()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let orientation: UIDeviceOrientation = size.height > size.width ? .portrait : .landscapeLeft
        configureSubviews(orientation)
    }
   @IBAction func nextButtonPressed(_ sender: UIButton) {
      setNewGame(gameMode: nameGame.gameMode)
   }
   
   @IBAction func generalButtonPressed(_ sender: UIButton) {
      dropDown.show()
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      for employee in nameGame.employees {
         employee.unsetImage()
      }
   }
}

extension NameGameViewController: NameGameDelegate {
   
}
