//
//  Employee.swift
//  NameGame
//
//  Created by Charlie  on 9/12/17.
//  Copyright © 2017 WillowTree Apps. All rights reserved.
//

import Foundation
import UIKit

class Employee {
   var id = ""
   var jobTitle = ""
   var firstName:String?
   var lastName:String?
   
   var headShot:UIImage?
   var headShotUrl:String?
   
   init(dictionary:[String:Any]) {
      if let id = dictionary[Keys.id] as? String {
         self.id = id
      }
      if let job = dictionary[Keys.jobTitle] as? String {
         self.jobTitle = job
      }
      firstName = dictionary[Keys.firstName] as? String
      lastName = dictionary[Keys.lastName] as? String
      if let url = (dictionary[Keys.headshot] as? [String:Any])?[Keys.url] as? String {
         headShotUrl = url.replacingOccurrences(of: "//", with: "http://")
      }
   }
   
   func getImage (completion:@escaping (_ image:UIImage?)->Void) {
      if let image = headShot {
         completion(image)
      }else if let urlString = headShotUrl, let url = URL(string: urlString) {
         DispatchQueue.global().async {
            let data = self.getData(fromUrl: url)
            DispatchQueue.main.async {
               if let data = data {
                  self.headShot = UIImage(data: data)
               }else {
                  self.headShot = #imageLiteral(resourceName: "anonymous-icon")
               }
               completion(self.headShot)
            }
         }
      }else {
         completion(#imageLiteral(resourceName: "anonymous-icon"))
      }
   }
   
   private func getData(fromUrl:URL)->Data? {
      do {
         let data = try Data(contentsOf: fromUrl)
         return data
      }catch {
         return nil
      }
   }
   
   func unsetImage() {
      headShot = nil
   }
   
   func getName()->String? {
      var name = firstName
      if let last = lastName?.characters.first {
         if let n = name {
            name = n + " " + "\(last)".uppercased() + "."
         }else {
            name = "\(last)".uppercased() + "."
         }
      }
      return name
   }
   struct Keys {
      static let firstName = "firstName"
      static let lastName = "lastName"
      static let headshot = "headshot"
      static let url = "url"
      static let id = "id"
      static let jobTitle = "jobTitle"
   }
}
