//
//  FaceButton.swift
//  NameGame
//
//  Created by Intern on 3/11/16.
//  Copyright © 2016 WillowTree Apps. All rights reserved.
//

import Foundation
import UIKit

open class FaceButton: UIButton {
   var employee:Employee!
   
    var id: Int = 0
    var tintView: UIView = UIView(frame: CGRect.zero)
   var nameLabel = UILabel(frame: CGRect.zero)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {

         imageView?.contentMode = .scaleAspectFill
        setTitleColor(.white, for: .normal)
        titleLabel?.alpha = 0.0

        tintView.alpha = 0.0
        tintView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tintView)

        tintView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        tintView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        tintView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tintView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      
      nameLabel.textColor = UIColor.white
      nameLabel.textAlignment = .center
      nameLabel.minimumScaleFactor = 0.5
      nameLabel.alpha = 0
      nameLabel.translatesAutoresizingMaskIntoConstraints = false
      addSubview(nameLabel)
      nameLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
      nameLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
      nameLabel.heightAnchor.constraint(equalToConstant: CGFloat(floatLiteral: 30.0)).isActive = true
      nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      
    }

    // TODO: Show the user's face on the button.
   
   func set(employee:Employee) {
      self.employee = employee
      alpha = 0.0
      tintView.alpha = 0.0
      nameLabel.alpha = 0
      nameLabel.text = employee.getName()
      employee.getImage { (image) in
         self.setImage(image, for: .normal)
         UIView.animate(withDuration: 1, animations: {
            self.alpha = 1
         })
      }
   }
   
   func set (correct:Bool) {
      if correct {
         tintView.backgroundColor = UIColor.green
      }else {
         tintView.backgroundColor = UIColor.red
      }
      UIView.animate(withDuration: 0.3) {
         self.nameLabel.alpha = 1.0
         self.tintView.alpha = 0.5
      }
   }
}
